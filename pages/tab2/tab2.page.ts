import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/interfaces/interfaces';
import { NewsService } from 'src/app/services/news.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  newsCategories = ['BUSINESS', 'ENTERTAINMENT', 'GENERAL']; //Me falla a pasar los datos
  mainCategory = 'business';
  articles: Article[] = [];

  constructor(private data: NewsService) {}

  ngOnInit(): void {
    this.loadNewsByCategory(this.newsCategories[0].toLowerCase(), false);
  }

  segmentChanged(event: any) {
    console.log('Segment changed', event.detail.value.toLowerCase());
    this.mainCategory = event.detail.value.toLowerCase();
    console.log(this.mainCategory, 'segment');
    this.articles = [];
    this.loadNewsByCategory(event.detail.value.toLowerCase(), false);
  }

  loadNewsByCategory(category: string, advance: boolean, event?) {
    if (advance) {
      this.data.getNewsByCategory(category).subscribe((response) => {
        console.log(`Category ${this.mainCategory} more news`, response);
        this.articles.push(...response.articles);

      });
      this.data.getNextPage();
    }

    this.data.getNewsByCategory(category).subscribe((response) => {
      console.log(`News by category ${this.mainCategory}`, response);
      this.articles.push(...response.articles);

      if (event) {
        event.target.complete();
      }
    });
  }

  loadData(event?) {
    console.log(this.mainCategory, 'loadData');
    this.loadNewsByCategory(this.mainCategory, true);
  }
}