import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { NewsResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root',
})

export class NewsService {
  page: number = 1;
  BASE_URL: string = `https://newsapi.org/v2/top-headlines?language=es`;
  categoryIndex: number = 0;

  constructor(private http: HttpClient) {}

  getNews() {
    return this.http.get<NewsResponse>(
      `${this.BASE_URL}&page=${this.page}&apiKey=${environment.apiKey}`
    );
  }

  getNewsByCategory(category: string) {
    return this.http.get<NewsResponse>(
      `${this.BASE_URL}&page=${this.page}&category=${category}&apiKey=${environment.apiKey}`
    );
  }

  getNextPage() {
    this.page++;
  }

  setCategoryIndex(newCatIndex: number) {
    this.categoryIndex = newCatIndex;
  }
}
